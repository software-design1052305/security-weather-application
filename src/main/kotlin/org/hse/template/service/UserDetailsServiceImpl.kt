package org.hse.template.service

import org.hse.template.converter.TimeConverter
import org.hse.template.model.User
import org.hse.template.model.WeatherServiceUserDetails
import org.hse.template.repository.UserRepository
import org.hse.template.repository.UserRoleRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import java.util.*


@Service
class UserDetailsServiceImpl(
    private val userRepository: UserRepository,
    private val userRoleRepository: UserRoleRepository,
    private val timeConverter: TimeConverter,
    @Value("\${user.password.expiration.time}") private val passwordExpirationTime: Double
) : UserDetailsService {
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user: Optional<User> = userRepository.findByName(username)
        return user.map { WeatherServiceUserDetails(it, userRoleRepository, timeConverter, passwordExpirationTime) }
            .orElseThrow { UsernameNotFoundException("$username not found") }
    }
}