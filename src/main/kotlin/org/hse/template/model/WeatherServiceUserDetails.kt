package org.hse.template.model

import org.hse.template.converter.TimeConverter
import org.hse.template.repository.UserRoleRepository
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDateTime


open class WeatherServiceUserDetails(
    private val user: User,
    private val userRoleRepository: UserRoleRepository,
    private val timeConverter: TimeConverter,
    private val passwordExpirationTime: Double
) : UserDetails {
    fun getUserId(): Long = user.id

    @Transactional
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> =
        userRoleRepository.findAuthorityTypesByUserId(user.id)

    override fun getPassword(): String = user.password

    override fun getUsername(): String = user.name

    override fun isAccountNonExpired(): Boolean {
        return LocalDateTime.now() <= user.passwordLastUpdate.plusMinutes(
            timeConverter.convertFromDaysToMinutes(
                passwordExpirationTime
            )
        )
    }

    override fun isAccountNonLocked() = true

    override fun isCredentialsNonExpired() = true

    override fun isEnabled() = true
}




