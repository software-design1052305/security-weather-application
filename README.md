# Сервис погоды с автоматическим обновлением

- weather REST методы:\
  GET /weather/{city} — запрос текущей погоды по названию города.\
  GET /forecast/{city} — запрос прогноза погоды на 5 дней.\
  GET /alerts/{city} — получение предупреждений о погодных условиях.\
  GET /update — автоматическое обновление данных о погоде по всем отслеживаемым\
  городам (используется с шедулером)

## Ссылки

- *SwaggerUI - http://localhost:8090/swagger-ui/index.html* 

Используется внешний API Weather Application, реализованный в https://gitlab.com/software-design1052305/weather-application .

## Для запуска:

1) в Weather Application в подпапке dev-env запустить docker контейнер командой:
```
docker-compose --project-name weather-app  up 
```
2) Запустить приложение Weather Application
3) в security-weather-application в подпапке dev-env запустить docker контейнер командой:
```
 docker-compose --project-name weather-application-main  up
```
4) Запустить приложение security-weather-application

Для запуска pgAdmin security-weather-application:
```
docker run -p 5050:80 -e 'PGADMIN_DEFAULT_EMAIL=user@example.com' -e 'PGADMIN_DEFAULT_PASSWORD=password' -v p
gadmin-data:/var/lib/pgadmin dpage/pgadmin4
```
