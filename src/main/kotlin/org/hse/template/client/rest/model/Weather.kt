package org.hse.template.client.rest.model

import io.swagger.v3.oas.annotations.media.Schema
import lombok.AccessLevel
import lombok.NoArgsConstructor
import java.time.LocalDateTime


@Schema(description = "Информация о погоде")
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
class Weather(
    var mainWeather: String,
    var weatherDescription: String,
    var temperature: Double,
    @Schema(description = "Атмосферное давление в гПа")
    var pressure: Int,
    @Schema(description = "Влажность в %")
    var humidity: Int,
    @Schema(description = "Время GMT")
    var dateTime: LocalDateTime,
    @Schema(description = "Скорость ветра в м/с")
    var windSpeed: Double
)