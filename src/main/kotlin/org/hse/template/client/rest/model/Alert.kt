package org.hse.template.client.rest.model

import io.swagger.v3.oas.annotations.media.Schema
import lombok.AccessLevel
import lombok.NoArgsConstructor
import java.time.LocalDateTime

@Schema(description = "Информация о погодных предупреждениях")
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
class Alert(
    var senderName: String,
    var event: String,
    var startDateTime: LocalDateTime,
    var endDateTime: LocalDateTime,
    var description: String,
)