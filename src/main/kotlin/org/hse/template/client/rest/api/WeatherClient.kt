package org.hse.template.client.rest.api

import io.swagger.v3.oas.annotations.media.Schema
import org.hse.template.client.rest.model.Alert
import org.hse.template.client.rest.model.Weather
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping

@Component
@FeignClient(name = "open-weather")
interface WeatherClient {

    @GetMapping("/weather/{city}")
    fun cityCurrentWeather(
        @Schema(
            description = "city Name",
            example = "London",
            type = "string"
        ) @PathVariable("city") city: String
    ): Weather

    @GetMapping("/forecast/{city}")
    fun cityFiveDaysForecast(@PathVariable("city") city: String): List<Weather>

    @GetMapping("/alerts/{city}")
    fun alerts(@PathVariable("city") city: String): List<Alert>

    @PostMapping("/update")
    fun update()
}
