package org.hse.template.controller

import org.hse.template.model.AuthorityType
import org.hse.template.model.UserRegistrationDetails
import org.hse.template.repository.UserRepository
import org.hse.template.service.UserService
import org.hse.template.validator.UserRoleCodeValidator
import org.hse.template.validator.UserValidator
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping


@Controller
class UserController(
    private val userService: UserService,
    private val userValidator: UserValidator,
    private val userRoleCodeValidator: UserRoleCodeValidator, private val userRepository: UserRepository
) {
    @GetMapping("/registration")
    fun showRegistrationForm(model: Model): String {
        val userRegistrationDetails = UserRegistrationDetails()
        userRegistrationDetails.roles = mutableListOf(AuthorityType.DEFAULT_USER)
        model.addAttribute("user_registration_details", userRegistrationDetails)
        model.addAttribute("error_message", "")
        return "registration"
    }

    @PostMapping("/registration/attempt")
    fun registration(userRegistrationDetails: UserRegistrationDetails, userRoleCode: String, model: Model): String {
        val role = userRoleCodeValidator.validate(userRoleCode)
        if (userRoleCode.isNotEmpty() && role == null) {
            model.addAttribute("error_message", "Код не существует.")
            model.addAttribute("user_registration_details", UserRegistrationDetails())
            return "registration"
        }
        if (role != null) {
            userRegistrationDetails.roles.add(role)
        }
        val message = StringBuffer()
        if (!userValidator.validate(userRegistrationDetails, message)) {
            model.addAttribute("error_message", message.toString())
            model.addAttribute("user_registration_details", UserRegistrationDetails())
            return "registration"
        }
        userService.save(userRegistrationDetails)
        return "redirect:/swagger-ui/index.html"
    }


    @GetMapping("/")
    fun welcome(model: Model?): String {
        return "redirect:/swagger-ui/index.html"
    }
}
