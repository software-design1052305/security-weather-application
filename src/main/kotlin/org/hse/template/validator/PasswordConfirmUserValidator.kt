package org.hse.template.validator

import org.hse.template.model.UserRegistrationDetails
import org.hse.template.service.UserService


class PasswordConfirmUserValidator(
    userService: UserService
) : BaseUserValidator(userService) {
    init {
        super.errorMessage = "Пароли должны совпадать"
    }

    override fun checkCondition(userRegistrationDetails: UserRegistrationDetails, message: StringBuffer): Boolean {
        return !userRegistrationDetails.passwordConfirmation.equals(userRegistrationDetails.password)
    }
}
