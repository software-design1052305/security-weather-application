package org.hse.template.model

import jakarta.persistence.*
import lombok.Data
import java.time.LocalDateTime


@Data
@Entity
@Table(name = "weather_service_user")
class User(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    var id: Long,
    @Column(unique = true)
    var name: String,
    var password: String,
    @Column(name = "password_last_update")
    var passwordLastUpdate: LocalDateTime
) {
    constructor() : this(0, "", "", LocalDateTime.now())
}