package org.hse.template.service

import lombok.AllArgsConstructor
import org.hse.template.model.AuthorityType
import org.hse.template.model.User
import org.hse.template.model.UserRegistrationDetails
import org.hse.template.model.UserRole
import org.hse.template.repository.UserRepository
import org.hse.template.repository.UserRoleRepository
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.time.LocalDateTime


@Service
@AllArgsConstructor
class UserServiceImpl(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder,
    private val userRoleRepository: UserRoleRepository
) : UserService {
    override fun save(userRegistrationDetails: UserRegistrationDetails) {
        val user = User()
        user.name = userRegistrationDetails.username
        user.password = passwordEncoder.encode(userRegistrationDetails.password)
        user.passwordLastUpdate = LocalDateTime.now()
        userRepository.save(user)
        saveUserRoles(user.id, userRegistrationDetails.roles)
    }

    override fun findByName(username: String?): User? {
        val optional = userRepository.findByName(username)
        return optional.orElse(null)
    }

    override fun deleteUserByName(username: String) {
        val user = userRepository.findByName(username)
        if (user.isPresent) {
            userRoleRepository.deleteAllById(user.get().id)
            userRepository.deleteById(user.get().id)
        }
    }

    fun saveUserRoles(userId: Long, authorities: List<AuthorityType>) {
        authorities.forEach { userRoleRepository.save(UserRole(userId, it)) }
    }
}