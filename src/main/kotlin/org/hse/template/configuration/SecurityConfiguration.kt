package org.hse.template.configuration

import org.hse.template.converter.TimeConverter
import org.hse.template.repository.UserRepository
import org.hse.template.repository.UserRoleRepository
import org.hse.template.service.UserDetailsServiceImpl
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.SecurityFilterChain


@Configuration
@EnableWebSecurity
@EnableMethodSecurity
class SecurityConfiguration(
    private val userRepository: UserRepository,
    private val userRoleRepository: UserRoleRepository,
    private val timeConverter: TimeConverter,
    @Value("\${user.password.expiration.time}") private val passwordExpirationTime: Double
) {

    @Bean
    fun userDetailsService(): UserDetailsService {
        return UserDetailsServiceImpl(userRepository, userRoleRepository, timeConverter, passwordExpirationTime)
    }

    @Bean
    @Throws(Exception::class)
    fun securityFilterChain(http: HttpSecurity): SecurityFilterChain {
        return http.csrf { it.disable() }
            .authorizeHttpRequests { auth ->
                auth.requestMatchers("/registration", "/login", "/registration/attempt").permitAll()
                    .requestMatchers("/**").authenticated()
            }
            .formLogin { form ->
                form.loginPage("/login").permitAll()
            }
            .build()
    }

    @Bean
    fun authenticationProvider(): AuthenticationProvider {
        val provider = DaoAuthenticationProvider()
        provider.setUserDetailsService(userDetailsService())
        provider.setPasswordEncoder(passwordEncoder())
        return provider
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }
}