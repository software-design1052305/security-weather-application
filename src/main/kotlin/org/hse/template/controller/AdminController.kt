package org.hse.template.controller

import org.hse.template.api.AdminApi
import org.hse.template.service.UserService
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class AdminController(
    private val userService: UserService
) : AdminApi {
    @PostMapping("/delete/{userName}")
    override fun deleteUserByName(@PathVariable("userName") userName: String) {
        userService.deleteUserByName(userName)
    }
}