package org.hse.template.configuration

import org.hse.template.service.UserService
import org.hse.template.validator.AlreadyRegisteredUserValidator
import org.hse.template.validator.PasswordConfirmUserValidator
import org.hse.template.validator.UserValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class UserValidationConfiguration(
    private val userService: UserService
) {

    @Bean
    fun userValidator(): UserValidator {
        val alreadyRegisteredUserValidator = AlreadyRegisteredUserValidator(userService)
        alreadyRegisteredUserValidator.setNext(PasswordConfirmUserValidator(userService))
        return alreadyRegisteredUserValidator
    }
}