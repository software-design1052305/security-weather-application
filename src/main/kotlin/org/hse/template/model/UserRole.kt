package org.hse.template.model

import jakarta.persistence.*
import java.io.Serializable

@Entity
@Table(name = "user_role")
@IdClass(UserRole::class)
data class UserRole(
    @Id
    @Column(name = "user_id")
    var userId: Long,
    @Id
    @Column(name = "authority_type")
    @Enumerated(EnumType.STRING)
    val authorityType: AuthorityType
) : Serializable