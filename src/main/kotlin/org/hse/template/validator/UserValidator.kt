package org.hse.template.validator

import org.hse.template.model.UserRegistrationDetails


interface UserValidator {
    fun setNext(userValidator: UserValidator)
    fun validate(userRegistrationDetails: UserRegistrationDetails, message: StringBuffer): Boolean
}
