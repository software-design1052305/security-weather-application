package org.hse.template.repository

import org.hse.template.model.AuthorityType
import org.hse.template.model.UserRole
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface UserRoleRepository : JpaRepository<UserRole?, Long?> {

    @Query(
        value = "SELECT ur.authority_type FROM user_role ur WHERE ur.user_id=:userId",
        nativeQuery = true
    )
    fun findAuthorityTypesByUserId(userId: Long): MutableList<AuthorityType>

    @Modifying
    @Transactional
    @Query(
        value = "DELETE FROM user_role ur WHERE ur.user_id=:userId",
        nativeQuery = true
    )
    fun deleteAllById(userId: Long)
}