\c

create user "postgres-user" with password 'postgres-password';

create database weather_security with owner = 'postgres';

\c weather_security

create schema ext;

grant usage on schema ext to "postgres-user";
alter role "postgres-user" set search_path = template, ext, public;

create extension "uuid-ossp" schema ext;
create schema template authorization "postgres-user";
