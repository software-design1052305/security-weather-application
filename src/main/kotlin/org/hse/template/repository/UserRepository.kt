package org.hse.template.repository

import org.hse.template.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : JpaRepository<User?, Long?> {
    fun findByName(username: String?): Optional<User>
    fun deleteByName(username: String?)
}