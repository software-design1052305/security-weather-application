package org.hse.template.service

import org.hse.template.model.User
import org.hse.template.model.UserRegistrationDetails


interface UserService {
    fun save(userRegistrationDetails: UserRegistrationDetails)
    fun findByName(username: String?): User?
    fun deleteUserByName(username: String)
}