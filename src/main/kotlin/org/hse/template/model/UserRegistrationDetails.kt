package org.hse.template.model

import lombok.NoArgsConstructor

@NoArgsConstructor
data class UserRegistrationDetails(
    val username: String = "",
    val password: String = "",
    val passwordConfirmation: String = "",
    var roles: MutableList<AuthorityType> = mutableListOf(AuthorityType.DEFAULT_USER)
)

