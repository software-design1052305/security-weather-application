package org.hse.template.validator

import org.hse.template.model.UserRegistrationDetails
import org.hse.template.service.UserService
import org.springframework.stereotype.Component


@Component
class AlreadyRegisteredUserValidator(
    userService: UserService
) : BaseUserValidator(userService) {
    init {
        super.errorMessage = "Пользователь с таким именем уже существует"
    }

    override fun checkCondition(userRegistrationDetails: UserRegistrationDetails, message: StringBuffer): Boolean {
        return super.userService.findByName(userRegistrationDetails.username) != null
    }
}