package org.hse.template.api

import io.swagger.v3.oas.annotations.Operation
import org.springframework.security.access.prepost.PreAuthorize

interface AdminApi {
    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(description = "Required role: ADMIN")
    fun deleteUserByName(
        userName: String
    )
}