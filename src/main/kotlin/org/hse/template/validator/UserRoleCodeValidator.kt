package org.hse.template.validator

import org.hse.template.model.AuthorityType
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component


@Component
class UserRoleCodeValidator(
    @Value("\${user.admin.secret.code}") private val adminSecreteCode: String,
    @Value("\${user.premium.secret.code}") private val premiumUsersSecretCode: String
) {
    fun validate(code: String): AuthorityType? {
        return AuthorityType.entries.find {
            when (it) {
                AuthorityType.ADMIN -> adminSecreteCode
                AuthorityType.PREMIUM_USER -> premiumUsersSecretCode
                else -> ""
            } == code
        }
    }
}
