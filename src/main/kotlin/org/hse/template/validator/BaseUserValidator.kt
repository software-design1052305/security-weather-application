package org.hse.template.validator

import org.hse.template.model.UserRegistrationDetails
import org.hse.template.service.UserService


abstract class BaseUserValidator(
    protected var userService: UserService,
    private var nextValidator: UserValidator? = null,
    protected var errorMessage: String? = null
) : UserValidator {

    protected abstract fun checkCondition(
        userRegistrationDetails: UserRegistrationDetails,
        message: StringBuffer
    ): Boolean

    override fun setNext(userValidator: UserValidator) {
        nextValidator = userValidator
    }

    override fun validate(userRegistrationDetails: UserRegistrationDetails, message: StringBuffer): Boolean {
        if (checkCondition(userRegistrationDetails, message)) {
            message.append(errorMessage)
            return false
        }
        return if (nextValidator == null) {
            true
        } else nextValidator!!.validate(userRegistrationDetails, message)
    }
}

