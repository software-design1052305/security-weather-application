package org.hse.template.converter

import org.springframework.stereotype.Component

@Component
class TimeConverterImpl : TimeConverter {
    override fun convertFromDaysToMinutes(days: Double): Long = (days * 24 * 60).toLong()
}