package org.hse.template.service

import org.hse.template.client.rest.model.Alert
import org.hse.template.client.rest.model.Weather
import org.springframework.stereotype.Service

@Service
interface WeatherService {
    fun cityCurrentWeather(cityName: String): Weather
    fun cityFiveDaysForecast(cityName: String): List<Weather>
    fun cityAlerts(cityName: String): List<Alert>

    fun update()
}