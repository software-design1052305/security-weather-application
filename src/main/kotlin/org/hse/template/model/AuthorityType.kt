package org.hse.template.model

import org.springframework.security.core.GrantedAuthority

enum class AuthorityType : GrantedAuthority {
    ADMIN,
    DEFAULT_USER,
    PREMIUM_USER;

    override fun getAuthority() = this.name
}