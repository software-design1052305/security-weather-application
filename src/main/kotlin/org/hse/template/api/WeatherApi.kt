package org.hse.template.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Schema
import org.hse.template.client.rest.model.Alert
import org.hse.template.client.rest.model.Weather
import org.springframework.security.access.prepost.PreAuthorize

interface WeatherApi {

    @PreAuthorize("hasAnyAuthority('PREMIUM_USER','ADMIN','DEFAULT_USER')")
    @Operation(description = "Required role: PREMIUM_USER/ADMIN/DEFAULT_USER")
    fun cityCurrentWeather(
        @Schema(
            description = "Название города",
            type = "string",
            example = "London"
        )
        city: String
    ): Weather

    @PreAuthorize("hasAnyAuthority('PREMIUM_USER','ADMIN')")
    @Operation(description = "Required role: PREMIUM_USER/ADMIN")
    fun cityFiveDaysForecast(
        @Schema(
            description = "Название города",
            type = "string",
            example = "London"
        )
        city: String
    ): List<Weather>

    @PreAuthorize("hasAnyAuthority('PREMIUM_USER','ADMIN','DEFAULT_USER')")
    @Operation(description = "Required role: PREMIUM_USER/ADMIN/DEFAULT_USER")
    fun alerts(
        @Schema(
            description = "Название города",
            type = "string",
            example = "London"
        )
        city: String
    ): List<Alert>

    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(description = "Required role: ADMIN")
    fun update(
    )
}